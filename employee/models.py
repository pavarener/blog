from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User


class Employee(models.Model):
    name = models.CharField(max_length=255)
    title = models.CharField(max_length=255)

    def __str__(self):
        return f"{self.name},{self.title}"


class BlogPosts(models.Model):
    title = models.CharField(max_length=255, null=False, blank=False)
    desc = models.TextField(null=False, blank=False)
    featured = models.BooleanField(default=False)
    image = models.ImageField(upload_to='images', null=True, blank=True)
    likes = models.ManyToManyField(User, related_name='blog_post')

    def total_likes(self):
        return self.likes.count()

    def __str__(self):
        return f"{self.title}"

    def __str__(self):
        return f"{self.title}"
