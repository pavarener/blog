from django.urls import path
from .import views
from .views import like

from django.views.generic import TemplateView
from users import views as user_views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', TemplateView.as_view(template_name='index.html'))
]
urlpatterns =[
    path('',views.blog, name='blog' ),
    path('details/<int:id>',views.detailsPage, name='details' ),
    path('create/',views.create, name='create' ),
    path('create/createData/',views.createData, name='createData' ),
    path('delete/<int:id>', views.delete, name='delete'),
    path('update/<int:id>',views.update, name='update' ),
    path('update/updateData/<int:id>',views.updateData, name='updateData' ),
    path('register/', user_views.register, name='register'),
    path('login/',
         auth_views.LoginView.as_view(template_name='users/login.html'),
         name='login'),
    path('logout/',
         auth_views.LogoutView.as_view(template_name='users/logout.html'),
         name='logout'),
    path('profile/', user_views.profile, name='profile'),
    path('like/<int:pk>', like, name='like_post'),

 
]