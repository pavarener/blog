
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from .models import Employee, BlogPosts
from django.db.models import Q
from django.http import HttpResponse


def like(request, pk):
    post = get_object_or_404(BlogPosts, id=request.POST.get('post_id'))
    post.likes.add(request.user)
    return HttpResponse(reverse('article_detail', args=[str(pk)]))


def index(request):
    if request.method == 'GET':
        myEmployees = Employee.objects.all().order_by('-name')
    elif request.method == 'POST':
        employee_name = request.POST.get('employee_name', '')
        myEmployees = Employee.objects.filter(name__icontains=employee_name).order_by('-name')

    template = loader.get_template('employee/index.html')
    context = {
        'myEmployees': myEmployees
    }
    return HttpResponse(template.render(context, request))

    
def create(request):
      template = loader.get_template('employee/createPage.html')
      return HttpResponse(template.render({},request))

def createData(request):
    data1 = request.POST['name']
    data2 = request.POST['title']
    newEmployee = Employee(name=data1, title= data2)
    newEmployee.save()
    return HttpResponseRedirect(reverse('employee'))

def delete(request,id):
    deleteEmployee = Employee.objects.get(id=id)
    deleteEmployee.delete()
    return HttpResponseRedirect(reverse('employee'))

def update(request,id):
      updatemployee = Employee.objects.get(id=id)
      template = loader.get_template('employee/updatePage.html')
      context = {
        'Employee': updatemployee
      }
      return HttpResponse(template.render(context,request))

def updateData(request, id):
    name = request.POST['name']
    title = request.POST['title']
    updatemployee = Employee.objects.get(id=id)
    updatemployee.name = name
    updatemployee.title = title
    updatemployee.save()
    return HttpResponseRedirect(reverse('employee'))  

def blog(request):
    if request.method == 'GET':
        posts = BlogPosts.objects.all().order_by('-title')
        blog_title = None
    elif request.method == 'POST':
        blog_title = request.POST.get('blog_title', '')
        posts = BlogPosts.objects.filter(title__icontains=blog_title).order_by('-title')
    featuredPost = BlogPosts.objects.filter(featured= True)
    template = loader.get_template('employee/blog.html')
    context = {
        'posts': posts,
        'blogtitlesearch': blog_title,
        'featuredPost': featuredPost,
    }



    return HttpResponse(template.render(context, request))

def detailsPage(request,id):
      detailsPost = BlogPosts.objects.get(id=id)
      template = loader.get_template('employee/detailsPage.html')
      context = {
        'posts': detailsPost
      }
      return HttpResponse(template.render(context,request))